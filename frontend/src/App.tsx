import React from 'react';
import './App.css';
import { PageType } from './types/PageType';
import { User } from './types/User'
import Start from './pages/start';
import Base from './pages/base';
import Settings from './pages/settings';
import { BrowserRouter as Router, Routes, Route }from 'react-router-dom';


type State = {
  currentPageType: PageType;
  videoDeviceId: (string|undefined);
};
type Props = unknown & {
  user: User
}

class App extends React.Component<Props, State>{
  constructor(props: Props){
    super(props);
    this.state = {
      currentPageType: PageType.START,
      videoDeviceId: undefined
    }
  }

  handleSelect = (pageType: PageType, user: User, videoDeviceId: (string|undefined)) => {
    // only update state when the requested page is different from the current one
    // only alow to request the first page when the current page is the last one 
    let pageTypeValues = Object.keys(PageType).filter((v) => !isNaN(Number(v))).map(Number);
    var isRequestedPageStartRequestedFromEnd = this.state.currentPageType === Math.max(...pageTypeValues) && pageType === Math.min(...pageTypeValues);
    if(this.state.currentPageType !== pageType && (this.state.currentPageType < pageType || isRequestedPageStartRequestedFromEnd)){
      console.log("Changing to Page " + pageType + " with videoDeviceId: " + videoDeviceId);
      this.setState({currentPageType: pageType, videoDeviceId: videoDeviceId})
    }
  }

  render() {
    var pageToDisplay;
    switch(this.state.currentPageType){
      case PageType.START:
        pageToDisplay = 
        <Start
          videoDeviceId = {this.state.videoDeviceId}
          proceedToNextStep={(user: User, videoDeviceId: (string|undefined)) => this.handleSelect(PageType.LOGIN, user, videoDeviceId)}
        />
        break;
      default:
        pageToDisplay = 
        <Base
          user = {this.props.user}
          videoDeviceId = {this.state.videoDeviceId}
          proceedToNextStep = {(user: User, videoDeviceId: (string|undefined), nextPage: PageType) => this.handleSelect(nextPage, user, videoDeviceId)}
          nextPage = {this.state.currentPageType}
        />
    }
    return (
      <>
        <Router>
          <Routes>
            <Route path='/' element={pageToDisplay} />
            <Route path='/settings' element={<Settings />}/>
          </Routes>
        </Router>
      </>
    );
  }

}

export default App;
