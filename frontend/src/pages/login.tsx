import React, { useEffect, useState } from "react";
import { useForm } from '../hooks/useForm';
import { User } from '../types/User';



type Props = {
  user: User;
  videoDeviceId: (string|undefined);
  proceedToNextStep: (user: User, videoDeviceId: (string|undefined)) => void;
};

interface Option {
  label: string;
  value: string;
}

export const Login: React.FC<Props> = (props) => {
  const {proceedToNextStep} = props;
  const [options, setOptions] = useState<Option[]>([]);
    // defining the initial state for the form
  const initialState = {
    firstName: '',
    lastName: '',
    company: '',
    email: '',
    interestedInMoreInfo: false
  };

  useEffect(() => {
    // Make the backend call to fetch data
    // For example, using fetch or axios
    // Replace 'backendApiUrl' with your actual backend API endpoint
    fetch('/api/getSpaceIndustryChoices')
      .then((response) => response.json())
      .then((data: Option[]) => {
        // Assuming the response contains an object with 'options' property
        setOptions(data);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  }, []);

  // getting the event handlers from our custom hook
  const { onChange, onSubmit } = useForm(
    loginUserCallback,
    initialState
  );

  // a submit function that will execute upon form submission
  async function loginUserCallback() {
    props.user.email = (document.getElementById('email') as HTMLInputElement)?.value;
    props.user.firstName = (document.getElementById('firstName') as HTMLInputElement)?.value;
    props.user.lastName = (document.getElementById('lastName') as HTMLInputElement)?.value;
    var emailDomain = (document.getElementById('email') as HTMLInputElement)?.value.split("@")[1].split(".")
    emailDomain.pop();
    props.user.company = emailDomain.join(".");
    props.user.industry = (document.getElementById('industry') as HTMLOptionElement)?.value;
    props.user.interestedInMoreInfo = (document.getElementById("interestedInMoreInfo") as HTMLInputElement)?.checked;
    // send 'values' to database
    proceedToNextStep(props.user, props.videoDeviceId);
  }

  return (
    <div className='App-form'>
    <p>Please enter your details to unlock an<br/>out-of-this-world experience:</p>
    <form  onSubmit={onSubmit} autoComplete="off">
      <div>
      <input type="hidden" value="prayer" />
      <input  className='App-input'
              type='First Name'
              name='firstName'
              id='firstName'
              placeholder='First Name*'
              onChange={onChange}
              autoComplete="off"
              required
              />
      <input  className='App-input'
              type='Last Name'
              name='lastName'
              id='lastName'
              placeholder='Last Name*'
              onChange={onChange}
              autoComplete="off"
              required
              />
      <input  className='App-input'
              type='email'
              name='email'
              id='email'
              placeholder='Email*'
              onChange={onChange}
              autoComplete="off"
              required
              />
        <select className='App-input input-selector' 
                id="industry" 
                placeholder="Industry*" 
                required
                onChange={(e) => e.target.id === "placeholer" ? document.getElementById("industry")!.style.color="gray":document.getElementById("industry")!.style.color="black"}>
          <option id="placeholder" value="" selected disabled>Industry*</option>
          {options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </select> 
      </div>
      <div className="checkbox-container">
        <div className="checkbox-left">
          <input  className='App-checkbox-input'
                  id="termsAndConditions"
                  type='checkbox'
                  required
          />
        </div>
        <div className="checkbox-label-right">I accept the <a href="https://www.privacypolicies.com/live/94bc34d8-b9e4-442b-94c3-5aac54902503" target="_blank" rel="noreferrer"
        className='App-link'>data privacy</a> agreement*</div>
      </div>
      <div className="checkbox-container">
        <div className="checkbox-left">
          <input  className='App-checkbox-input'
                  id="interestedInMoreInfo"
                  type='checkbox'
          />
        </div>
        <div className="checkbox-label-right">I'm interested in receiving more information about innovative Satellite Components (Release Actuators, Deployables, Mechanisms).</div>
      </div>
      <button type='submit' className='App-button'> Go!</button>
    </form>
    </div>
  );
}


export default Login