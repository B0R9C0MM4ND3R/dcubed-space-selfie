import React from "react";
import { User } from "../types/User";
import logo from '../assets/logo.png';

type Props = {
    videoDeviceId: (string|undefined);
    proceedToNextStep: (user: User, videoDeviceId: (string|undefined)) => void;
  };


  
  export const Start: React.FC<Props> = (props) => {
    const {videoDeviceId, proceedToNextStep} = props;

    var newUser: User={
        firstName: '',
        lastName: '',
        company: '',
        email: '',
        industry: '',
        interestedInMoreInfo: false
    }

    const ClickKeyCallback = () => {
        window.removeEventListener('click', ClickKeyCallback);
        window.removeEventListener('keydown', ClickKeyCallback);
        proceedToNextStep(newUser, videoDeviceId);
    }

    window.addEventListener('keydown', ClickKeyCallback);
    window.addEventListener('click', ClickKeyCallback);

    return(
        <div className='App Start'>  
            <div><img src={logo} className='App-logo' alt='logo' /></div>
            <div className="Welcome">Welcome to DCUBED's Space Selfie Stick</div>
            <div className="HelpText">Press anywhere to try it out!</div>
        </div>
    );
}

export default Start
