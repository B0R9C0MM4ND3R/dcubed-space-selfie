import React , { useState } from "react";
import logo from '../assets/logo.png';
import { User } from '../types/User'
import Login from "./login";
import Selfie from "./selfie";
import Close from "./close";
import { PageType } from "../types/PageType";

type Props = {
    user: User;
    videoDeviceId: (string|undefined);
    proceedToNextStep: (user: User, videoDeviceId: (string|undefined), nextPage: PageType) => void;
    nextPage: PageType;
  };



export const Base: React.FC<Props> = (props) => {
    const {user, videoDeviceId, proceedToNextStep} = props;
    const [currentPageType, setCurrentPageType] = useState(PageType.LOGIN);
    const [backendOnline, setBackendOnline] = useState(true);

    var pageToDisplay;
    var textToDisplay: String = "";
    var helpText: String = "";

    var newUser: User={
        firstName: '',
        lastName: '',
        company: '',
        email: '',
        industry: '',
        interestedInMoreInfo: false
    }

    const handleSelect = (pageType: PageType, user: User, videoDeviceId: (string|undefined)) => {
        // only update state when the requested page is different from the current one
        if(currentPageType !== pageType){
          console.log("Changing to PageSection " + pageType + " with videoDeviceId: " + videoDeviceId);
          setCurrentPageType(pageType);
          proceedToNextStep(user, videoDeviceId, pageType);
          //this.setState({currentPageType: pageType, videoDeviceId: videoDeviceId})
        }
      }

    const handleLogoClick = () =>{
        if(currentPageType === PageType.CLOSE){
          handleSelect(PageType.START, newUser, videoDeviceId);
        }
    }

    switch(currentPageType){
        case PageType.LOGIN:
          pageToDisplay = 
          <Login 
            proceedToNextStep={(user: User, videoDeviceId: (string|undefined)) => handleSelect(PageType.SELFIE, user, videoDeviceId)}
            user={user}
            videoDeviceId = {videoDeviceId}/>
          textToDisplay = ""
          break;
        case PageType.SELFIE:
          pageToDisplay = 
          <Selfie
            proceedToNextStep={(user: User, videoDeviceId: (string|undefined)) => handleSelect(PageType.CLOSE, user, videoDeviceId)}
            user={user} 
            videoDeviceId = {videoDeviceId}/>
            textToDisplay = ""
            helpText="Press the camera button to start the countdown!";
            break;
        case PageType.CLOSE:
          pageToDisplay = 
          <Close 
            proceedToNextStep={( videoDeviceId: (string|undefined)) => handleSelect(PageType.START, newUser, videoDeviceId)}
            user = {user}
            videoDeviceId = {videoDeviceId}/>
            textToDisplay = "It was great to see you!"
            helpText="Press anywhere to start over.";
          break;
      }

    const checkBackendOnline = async () => {
      try{
        var res = await fetch('/api/ping')
        setBackendOnline(res.status === 200);
        return;
      } catch (e: any){
        setBackendOnline(false);
        return;
      }
    }

    setInterval(() => checkBackendOnline(), 5000);

    return(
        <div className='App'>
          <div className='App-Col'>
            <img src={logo} onClick={handleLogoClick}  className='App-logo' alt='logo' />
            <p>
              {textToDisplay}
            </p>
            <p>
              #DCUBEDspace
            </p>
          </div>
          <div className='App-Col'>
            {pageToDisplay}
          </div>
        <div className="HelpText">{helpText}</div>
        {!backendOnline && <div className="HelpText">Backend offline</div>}
      </div>
    );
}

export default Base