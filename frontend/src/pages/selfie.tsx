import React, { useRef, useState} from 'react';
import { User } from '../types/User';
import photoImg from '../assets/photo.png';
import retakeImg from '../assets/refresh.png';
import launchImg from '../assets/launch.png';
import { CountdownCircleTimer, TimeProps } from 'react-countdown-circle-timer'
import { Config } from '../types/Config';
import {useCookies} from 'react-cookie';

declare global {
    interface Window{
        stream: MediaStream;
        config: Config;
    }
}

type Props = {
    user: User;
    videoDeviceId: (string|undefined);
    proceedToNextStep: (user: User, videoDeviceId: (string|undefined)) => void;
}

function timeout(delay: number) {
    return new Promise( res => setTimeout(res, delay) );
}
  
  export const Selfie: React.FC<Props> = (props) =>{
    const {user, proceedToNextStep} = props;
    const [imgSrc, setImgSrc] = useState('');
    const [countdown, setCountdown] = useState(false);
    const [launch, setLaunch] = useState(false);
    const [videoDeviceIdSelected, setVideoDeviceIdSelected] = useState("");
    const [videoClass, setVideoClass] = useState("videoStream");
    const [finishedSelfie, setFinishedSelfie] = useState(false);
    const [intentionallyStopped, setIntentionallyStopped] = useState(false)
    const [cookies] = useCookies();
    
    let videoElement: any = document.querySelector('video');
    let videoSelect: any = document.querySelector('select#videoSource');

    var videoDeviceIdSavedFromLastTime = props.videoDeviceId;

    const isImageMirrored =  JSON.parse(cookies.mirrorImage);
    const cookieDeviceId =  cookies.videoDeviceId;
    const showImageTime = cookies.showImageTime ?? window.config.showImageTime
    const launchCountdownTime = cookies.launchCountdownTime ?? window.config.countdownTime
      
    function gotDevices(deviceInfos: MediaDeviceInfo[]) {
        // Handles being called several times to update labels. Preserve values.
        console.log("found " + deviceInfos.length + " media devices");
        console.log(deviceInfos)
        const values = videoSelect !== null ?videoSelect.value: undefined;
        // clear select to repopulate
        while (videoSelect !== null && videoSelect.firstChild) {
            videoSelect.removeChild(videoSelect.firstChild);
        }
        // populate 
        for (let i = 0; i !== deviceInfos.length; ++i) {
            const deviceInfo = deviceInfos[i];
            if (deviceInfo.kind === 'videoinput') {
            const option = document.createElement('option');
            option.value = deviceInfo.deviceId;
                option.text = deviceInfo.label || `camera ${videoSelect.length + 1}`;
                videoSelect.appendChild(option);
            }
        }
        
        if (Array.prototype.slice.call(videoSelect.childNodes).some(n => n.value === values)) {
            videoSelect.value = values;
        }
          
    }

    function gotStream(stream: MediaStream) {
        window.stream = stream; // make stream available to console
        videoElement.srcObject = stream;
        // Refresh button list in case labels have become available
        return navigator.mediaDevices.enumerateDevices();
    }
      
    function handleError(error: any) {
        console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
    }

      
      
    const capture = React.useCallback(
         async () => {
            const canvas = document.createElement("canvas");
            canvas.width = 1920;
            canvas.height = 1080;
            const context = canvas.getContext("2d");
            if(context && canvas){
                if(isImageMirrored){
                    context.translate(canvas.width, 0);
                    context.scale(-1, 1);
                }
                context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
            }
            const imgSrc = canvas.toDataURL("image/png", 1);
            setImgSrc(imgSrc);
            //downloadBase64File(imgSrc, 'test.png');
            console.log(user);
             
            stopAllStreams();
            setIntentionallyStopped(true);
        },
        [videoElement, setImgSrc, user, isImageMirrored]
    );

    function stopAllStreams(){
        console.log("stop");
        if (window.stream) {
            window.stream.getTracks().forEach(track => {
            track.stop();
            });
        }
    }

    function start() {
        if(intentionallyStopped){
            return
        }
        console.log("start");
        videoSelect = document.querySelector('select#videoSource');
        videoElement = document.querySelector('video');
        if(isImageMirrored){
            setVideoClass("videoStream flip");
        } else {
            setVideoClass("videoStream");
        }
        navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(handleError);
        const videoSource = videoSelect !== null ? videoSelect.value : undefined;
        let deviceId = videoSource;
        if(cookieDeviceId!==undefined && cookieDeviceId !== ""){
            console.log("device-ID override via cookie: " + cookieDeviceId)
            deviceId = cookieDeviceId
        }

        const constraints = {
            video: {
                deviceId: deviceId,
                 width: { ideal: 1920 },
                height: { ideal: 1080 }
            } // , width: {ideal: window.config.videoIdealWith}
        };
        if(deviceId){
            setVideoDeviceIdSelected(deviceId);
            console.log("saved videoDeviceId: " + deviceId);
        }
        navigator.mediaDevices.getUserMedia(constraints).then(gotStream).then(gotDevices).catch(handleError);
      }


    function sendSelfie(){
        fetch('/api/add_contact', {
            method: 'POST',
            headers: {"Content-Type": "application/json" },
            body: JSON.stringify({
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    company: user.company,
                    spaceindustry: user.industry,
                    interestedInMoreInfo: user.interestedInMoreInfo,
                    image: imgSrc,
                }),
            }
        ).then(r=>r.json());
    }

    const RenderTime = ({remainingTime}:TimeProps) => {
        const currentTime = useRef(remainingTime);
        const prevTime = useRef(currentTime.current +1);
        const isNewTimeFirstTick = useRef(false);
        const [, setOneLastRerender] = useState(0);
      
        if (currentTime.current !== remainingTime) {
          isNewTimeFirstTick.current = true;
          prevTime.current = currentTime.current;
          currentTime.current = remainingTime;
        } else {
          isNewTimeFirstTick.current = false;
        }
      
        // force one last re-render when the time is over to tirgger the last animation
        if (remainingTime === 0) {
              setTimeout(() => {
                  setOneLastRerender((val) => val + 1);
              }, 20);
        }
      
        const isTimeUp = isNewTimeFirstTick.current;
      
        return (
          <div className="time-wrapper">
            <div key={remainingTime} className={`time ${isTimeUp ? "up" : ""}`}>
              {remainingTime}
            </div>
            {prevTime.current !== null && (
              <div
                key={prevTime.current}
                className={`time ${!isTimeUp ? "down" : ""}`}
              >
                {prevTime.current}
              </div>
            )}
          </div>
        );
      };

    return(
        <div>
            <div>
                <div className="camView" onLoad={() => start()}>
                    {!launch && <video className={videoClass} id="video" playsInline autoPlay></video>}
                    {launch && <img  className="videoStream" src={imgSrc} alt="selfie"/>}
                    {!launch && !countdown && <img className="photoButton" src={photoImg} alt="takePhoto" onClick={() => setCountdown(true)}/>} 
                    {!launch && countdown && <div className="countdownCircleTimer">
                        <CountdownCircleTimer
                            isPlaying
                            duration={launchCountdownTime}
                            colors={["#004777", "#F7B801", "#A30000", "#A30000"]}
                            colorsTime={[(launchCountdownTime/4)*3, (launchCountdownTime/4)*2, (launchCountdownTime/4), 0]}
                            onComplete={() => {
                                // do your stuff here
                                capture();
                                setLaunch(true);
                            }}
                            >
                            {RenderTime}
                        </CountdownCircleTimer>
                    </div> }
                    {launch && !finishedSelfie && 
                    <div className='launch'> 
                        <figure className='figure'>
                            <img className="photoButton" alt="Retake" src={retakeImg}
                                onClick={() => {
                                    setIntentionallyStopped(false);
                                    start();                            
                                    setLaunch(false);
                                    setCountdown(false);
                                    }
                                }/>
                            <figcaption>Retake</figcaption>
                        </figure>
                        <figure className='figure'>
                            <img className="photoButton" alt="Launch" src={launchImg} 
                                onClick={async() => {
                                    var deviceIdToSaveForNextTime = videoDeviceIdSavedFromLastTime !== undefined && videoDeviceIdSavedFromLastTime !== "" 
                                    ? videoDeviceIdSavedFromLastTime 
                                    : videoDeviceIdSelected !== "" ? videoDeviceIdSelected : undefined
                                    setFinishedSelfie(true);
                                    sendSelfie();
                                    await timeout(showImageTime * 1000);
                                    proceedToNextStep(user,  deviceIdToSaveForNextTime);
                                    }
                                }/>
                            <figcaption>Launch</figcaption>
                        </figure>
                    </div>}
                    {launch && finishedSelfie && <div>LAUNCH!</div>}
                </div>
            </div>
        </div>
    )
}

export default Selfie