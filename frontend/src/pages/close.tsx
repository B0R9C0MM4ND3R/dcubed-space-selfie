import { User } from '../types/User';
import qrCode from '../assets/qr.png';
import { Config } from '../types/Config';
import {useCookies} from 'react-cookie';

type Props = {
    user: User;
    videoDeviceId: (string|undefined);
    proceedToNextStep: (videoDeviceId: (string|undefined)) => void;
  };

declare global {
    interface Window{
        stream: MediaStream;
        config: Config;
    }
}

export const Close: React.FC<Props> = (props) => {
    const {user, videoDeviceId, proceedToNextStep} = props;
    const [cookies] = useCookies();

    const reloadTime = cookies.reloadTime ?? window.config.reloadTime;
    
    const ClickKeyCallback = () => {
        window.removeEventListener('click', ClickKeyCallback);
        window.removeEventListener('keydown', ClickKeyCallback);
        stopAllStreams();
        proceedToNextStep(videoDeviceId); 
    }

    window.addEventListener('keydown', ClickKeyCallback);
    window.addEventListener('click', ClickKeyCallback);

    setTimeout(()=>{
        stopAllStreams();
        proceedToNextStep(videoDeviceId)}, reloadTime * 1000
    );    

    function stopAllStreams(){
        if (window.stream) {
            window.stream.getTracks().forEach(track => {
            track.stop();
            });
        }
    }

    return(
        <div>
            <p>
            Thanks for stopping by {user.firstName}! <br/>
            We'll be sending over your
            SpaceSelfie shortly.
            </p>
            <p>
            To learn more about our
            products, visit:
            </p>
            <img src={qrCode} className='QR-code' alt='qrCode' />
            <div className='MadeBy'>Created by Christoph Stemp <br/>christoph@stemp-dev.de</div>
        </div>
    );
}

export default Close