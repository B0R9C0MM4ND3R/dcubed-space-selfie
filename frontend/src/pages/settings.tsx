import React, { ChangeEvent, useEffect, useRef, useState } from 'react';
import Button from 'react-bootstrap/esm/Button';
import Col from 'react-bootstrap/esm/Col';
import Form from 'react-bootstrap/esm/Form';
import ListGroup from 'react-bootstrap/esm/ListGroup';
import Row from 'react-bootstrap/esm/Row';
import { useCookies } from 'react-cookie';


export const Settings: React.FC = () => {

    const [cookies, setCookie] = useCookies(['videoDeviceId', 'mirrorImage', 'launchCountdownTime', 
                                    'showImageTime', 'reloadTime']);
    const [videoClass, setVideoClass] = useState("videoStream");
    const [backendSettings, setBackendSettings] = useState([{key: 'key', value: 'value', caption:'caption'}]);
    const  isFirst = useRef(true);
    const saveCookies = (videoDeviceId: String) => {
        if(videoDeviceId !== ''){
            setCookie('videoDeviceId', videoDeviceId, { path: '/' });
        }
        setCookie('mirrorImage', getMirrorImage().checked, { path: '/' });
        setCookie('launchCountdownTime', getLaunchCountdownTime()?.value, { path: '/' });
        setCookie('showImageTime', getShowImageTime()?.value, { path: '/' });
        setCookie('reloadTime', getReloadTime()?.value, { path: '/' });
     };

    useEffect(() => {
        if(isFirst.current){
            isFirst.current = false;
            const mirrorImage: any = getMirrorImage();
            mirrorImage.checked = JSON.parse(cookies.mirrorImage ?? false);
            mirrorImageChange();
            getLaunchCountdownTime().value = cookies.launchCountdownTime ?? window.config.countdownTime;
            getShowImageTime().value = cookies.showImageTime ?? window.config.showImageTime;
            getReloadTime().value = cookies.reloadTime ?? window.config.reloadTime;
            fetchBackendSettings();
        }
        enumerateDevices();
    });

    function enumerateDevices(){
        navigator.mediaDevices.enumerateDevices().then(gotDevices);
    }

    function gotDevices(deviceInfos: MediaDeviceInfo[]) {
        const videoSelect: any = document.querySelector('select#videoSource');
        // Handles being called several times to update labels. Preserve values.
        const values = videoSelect !== null ? videoSelect.value : null;
        
        if (values === null){
            return null;
        }
        while (values !== null && videoSelect!.firstChild) {
            videoSelect!.removeChild(videoSelect!.firstChild);
        }
        
        for (let i = 0; i !== deviceInfos.length; ++i) {
            const deviceInfo = deviceInfos[i];
            if (deviceInfo.kind === 'videoinput') {
                const option = document.createElement('option');
                option.value = deviceInfo.deviceId;
                option.text = deviceInfo.label || `camera ${videoSelect.length + 1}`;
                videoSelect!.appendChild(option);
            }
        }
        
        if (Array.prototype.slice.call(videoSelect!.childNodes).some(n => n.value === values)) {
            videoSelect.value = values;
        }
        startPreview();
    }

    function saveFrontend(){
        const videoSelect: any = document.querySelector('select#videoSource');
        saveCookies(videoSelect.value);
        alert("Frontend saved successfully")
    }

    function videoSourceChange(){
        const videoSelect: any = document.querySelector('select#videoSource');
        const videoElement: any = document.querySelector('video');
        if(window.stream){
            window.stream.getTracks().forEach(track => track.stop());
        }
        const constraints = {
            video: {deviceId: videoSelect.value}}
        navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
            if(stream !== null){
                window.stream = stream;
                if(videoElement !== null){
                    videoElement.srcObject = stream;
                }
            }
        });
    }

    function mirrorImageChange(){
        const mirrorImage: any = getMirrorImage();
        if(mirrorImage.checked){
            setVideoClass("videoStream flip");
        } else {
            setVideoClass("videoStream");
        }
    }

    function getMirrorImage(): any{
        return document.querySelector('#mirrorImage');
    }

    function getLaunchCountdownTime(): any{
        return document.querySelector('#launchCountdownTime');
    }

    function getShowImageTime(): any{
        return document.querySelector('#showImageTime');
    }

    function getReloadTime(): any{
        return document.querySelector('#reloadTime');
    }

    function startPreview(){
        const videoSelect: any = document.querySelector('select#videoSource');
        if(videoSelect.value === undefined || videoSelect.value === null || videoSelect.value === ""){
            return;
        }
        if(cookies.videoDeviceId !== undefined && cookies.videoDeviceId !== ""){
            videoSelect.value = cookies.videoDeviceId;
        }
        videoSourceChange();
    }

    function stopPreview(){
        if(window.stream){
            window.stream.getTracks().forEach(track => track.stop());
        }
    }


    function fetchBackendSettings(){
        fetch('api/settings', {
            method: 'GET',
        }).then(r=> {
            return r.json()
        }).then(d => {
            setBackendSettings(d);
        });
    }

    function saveBackendSettings(){
        fetch('api/settings',{
            method: 'PUT',
            headers: {"Content-Type": "application/json" },
            body: JSON.stringify(backendSettings),
        }).then(r=> {
            return r.json();
        }).then(d=>{
            if(d === 'success'){
                alert("Backend saved successfully");
            } else {
                alert("Backend save unsuccessfull, check Backend server!");
            }
        });
    }

    function handelBackendSettingsOnChange(event: ChangeEvent){
        let key = (event.target as HTMLInputElement).id;
        let newVal = (event.target as HTMLInputElement).value;
        setBackendSettings(prevState => (
            prevState.map(
                el => el.key === key? { ...el, value: newVal}: el
            )
        ));
    }


    return(
    <div className='container my-5'>
        <h1>Settings</h1>
        <div key="frontendSettings"  className='mt-5 mt-2'>
            <h3>Frontend-Settings:</h3> 
            <div>
                <ListGroup>
                    <ListGroup.Item>
                        <h6>Video source:</h6>
                        <Row>
                            <Col md={10}><Form.Select id="videoSource" onChange={videoSourceChange}></Form.Select></Col>
                            <Col md={2}><Button onClick={() => {enumerateDevices();}}>Refresh</Button></Col>
                        </Row>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <h6>Mirror Image</h6>
                        <Form.Check type='switch' id='mirrorImage' onChange={() =>mirrorImageChange()}></Form.Check>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <h6 onLoad={() =>startPreview()}>Preview:</h6>
                        <div className='mb-2'>
                            <Button className='ml-5' onClick={startPreview}>Start preview</Button>
                            <Button className='mr-5'onClick={stopPreview}>Stop preview</Button>
                        </div>
                        <video className={videoClass} id="video" playsInline autoPlay></video>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <h6>Launch countdown time (seconds):</h6>
                        <Form.Control id='launchCountdownTime' type='number' defaultValue={window.config.countdownTime}></Form.Control>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <h6>Show image time (seconds):</h6>
                        <Form.Control id='showImageTime' type='number' defaultValue={window.config.showImageTime}></Form.Control>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <h6>Last page reload time (seconds):</h6>
                        <Form.Control id='reloadTime' type='number' defaultValue={window.config.reloadTime}></Form.Control>
                    </ListGroup.Item>

                </ListGroup>
            </div> 
        </div>
        <Button onClick={saveFrontend}>Save frontend settings</Button>
        
        <div key="backendSettings"  className='mt-5 mt-2'>
            <h3>Backend-Settings:</h3>
            <div>
                <ListGroup>
                    {backendSettings.map(element => {
                        return(<ListGroup.Item key={element.key}>
                            <h6>{element.caption}</h6>
                            <Form.Control 
                                id={element.key} 
                                defaultValue={element.value} 
                                onChange={(event) => handelBackendSettingsOnChange(event)}></Form.Control>
                        </ListGroup.Item>);
                    })}
                </ListGroup>
            </div>
        </div>
        <Button onClick={saveBackendSettings}>Save backend settings</Button>
       </div>
    );
    
}

export default Settings;