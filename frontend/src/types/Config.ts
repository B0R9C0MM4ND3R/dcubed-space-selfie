export type Config = {
    reloadTime: number;
    countdownTime: number;
    showImageTime: number;
}