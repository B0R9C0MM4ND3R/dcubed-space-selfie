export type User = {
    firstName: string;
    lastName: string;
    company: string;
    email: string;
    industry: string;
    interestedInMoreInfo: boolean;
}