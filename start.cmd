@echo off
pushd .
for /d %%i in (.\*) do call :$DoSomething "%%i"
popd

pause
exit /B

::**************************************************
:$DoSomething
::**************************************************

::echo current directory: %1
set "TRUE="
if %1 ==".\backend" set TRUE=1
if %1 ==".\frontend" set TRUE=1
if defined TRUE (
    cd %1
    echo starting: %1
    start cmd.exe /k "npm install && npm start"
    cd ..
)

exit /B