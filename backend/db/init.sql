-- SQLite
CREATE TABLE settings (key varchar(255) NOT NULL, value varchar(255) NOT NULL, caption varchar(255))

Insert into settings(key, value, caption) Values
    ('contactsFolder', 'contacts/', 'Contacts Folder Path'),
    ('contactsFileName', 'contacts.csv', 'Contacts File Name'),
    ('hubspotNoteText', 'Took Space Selfie at SmallSat2022.', 'Hubspot Note Text'),
    ('hubspotImageUploadBaseFolder', 'SmallSat2022', 'Hubspot Image Upload Folder Name'),
    ('spaceIndustryPropertyObjectType', 'contacts', 'SpaceIndustry Property ObjectType'),
    ('spaceIndustryPropertyName', 'SpaceIndustry','SpaceIndustry Property Name'),
    ('imagePrefix', 'DCUBED', 'Imagename Prefix'),
    ('UrlShortDomain', 'spaceselfie.dcubed-space.com', 'Url Shortener Domain')

