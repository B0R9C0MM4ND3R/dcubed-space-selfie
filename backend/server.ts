import express from 'express';
import * as hubspot from '@hubspot/api-client'
import { SimplePublicObjectInput, SimplePublicObjectInputForCreate } from '@hubspot/api-client/lib/codegen/crm/contacts';
import { AssociationSpec, PublicObjectSearchRequest } from '@hubspot/api-client/lib/codegen/crm/companies';
import FormData from 'form-data';
import { Moment } from 'moment';
import moment from 'moment';
import axios from 'axios';
import Database from 'better-sqlite3';

const app = express();
const port = process.env.PORT || 5000;
const HUBSPOT_API_KEY = process.env.HUBSPOT_API_KEY;
const REBRANDLY_API_KEY = process.env.REBRANDLY_API_KEY;
const DATE_TIME_FORMAT = 'MM-DD-YYYY HH:mm:ss';
const settings = {
  CONTACTS_FOLDER: 'contactsFolder',
  CONTACTS_FILE_NAME: 'contactsFileName',
  HUBSPOT_NOTE_TEXT: 'hubspotNoteText',
  HUBSPOT_IMAGE_UPLOAD_BASE_FOLDER: 'hubspotImageUploadBaseFolder',
  SPACE_INDUSTRY_PROPERTY_OBJECT_TYPE: 'spaceIndustryPropertyObjectType',
  SPACE_INDUSTRY_PROPERTY_NAME: 'spaceIndustryPropertyName',
  IMAGE_PREFIX: 'imagePrefix',
  URL_SHORT_DOMAIN: 'UrlShortDomain',

}

let db = new Database('./db/spaceSelfie.db');

app.use(express.json({limit: '50mb'}));
app.listen(port, () => console.log(`Listening on port ${port}`));

const hubspotClient: hubspot.Client = new hubspot.Client({ accessToken: HUBSPOT_API_KEY, 
    defaultHeaders: { 'Access-Control-Allow-Origin': '*'},})

app.get('/api/settings', async(req, res) => {
  let selectResult = db.prepare('SELECT * FROM settings').all();
  res.status(200).json(selectResult);
});

app.put('/api/settings',async (req, res) => {
  const settings = req.body;
  settings.map((s: { value: string; key: string; }) =>{
    db.prepare("UPDATE settings SET value='" + s.value + "' WHERE key='" + s.key +  "'").run();
  });
  res.status(200).json('success');
});

app.get('/api/getSpaceIndustryChoices', async (req, res) => {
  try{
    const apiResponse = await hubspotClient.crm.properties.coreApi.getByName(getSetting(settings.SPACE_INDUSTRY_PROPERTY_OBJECT_TYPE), getSetting(settings.SPACE_INDUSTRY_PROPERTY_NAME));
    const result:{label:string, value:string}[] = apiResponse.options.map(({label, value}) => ({label, value}));
    res.send(result);
  } catch (e: any){
    e.message === 'HTTP request failed'
    ? console.error(JSON.stringify(e.response, null, 2))
    : console.error(e)
  }
});

app.post('/api/add_contact', async (req, res) => {
  const { firstName, lastName, email, company, spaceindustry, interestedInMoreInfo, image } = req.body;
  let now: Moment;
  now = moment(new Date(), DATE_TIME_FORMAT);
  saveLocalCopyOfContact(firstName, lastName, email, company, spaceindustry, interestedInMoreInfo, now, image);

  var contactId: (string|undefined) = await createContact(firstName, lastName, email, spaceindustry, interestedInMoreInfo);
  if(contactId !== undefined){
    await createCompany(company, contactId);
    var imageIdAndUrl: ([string,string]|undefined) = await uploadImage(image, lastName, firstName, company, contactId, now);
    if(imageIdAndUrl !== undefined){
      var imageId = imageIdAndUrl[0];
      var imageUrl = imageIdAndUrl[1];
      imageUrl = await shortenUrl(imageUrl);
      await addImageUrlToContact(contactId, imageUrl);
      await linkImageToContact(imageId, contactId);
    }
  }
  res.send({contactId: contactId});
});

app.get('/api/ping', (req, res) => {
  res.send("pong");
})

app.post('/api/test/search_company', (req, res) => {
  const {companyName} = req.body;
  const publicObjectSearchRequest: PublicObjectSearchRequest = { 
    filterGroups: [
      {"filters":[{"value":companyName,"propertyName":"name","operator":"EQ"}]}
    ], 
    sorts: ["name"], 
    properties: ["ascending"], 
    limit: 100, 
    after: 0,
  };
  try{
    hubspotClient.crm.companies.searchApi.doSearch(publicObjectSearchRequest).then(r=>res.send(r))
  } catch (e: any) {
    e.message === 'HTTP request failed'
      ? console.error(JSON.stringify(e.response, null, 2))
      : console.error(e)
  }
}); 

app.post('/api/test/create_folders', (req, res) => {
  const {companyName} = req.body;
  createFoldersForUpload(companyName).then(r=>res.send(r));
});

async function getCompanyId(companyName: string){
  var result = await getCompanyByName(companyName);
  if(result !== undefined && result.results.length > 0){
    return result.results[0].id
  }
  return undefined
}

async function doesCompanyAlreadyExist(companyName: string){
  var result = await getCompanyByName(companyName);
  return result !== undefined && result.results.length >0
}

async function getCompanyByName(companyName: string){
  const publicObjectSearchRequest: PublicObjectSearchRequest = { 
    filterGroups: [
      {"filters":[{"value":companyName,"propertyName":"name","operator":"EQ"}]}
    ], 
    sorts: ["name"], 
    properties: ["ascending"], 
    limit: 100, 
    after: 0,
  };

  try{
    var result  = await hubspotClient.crm.companies.searchApi.doSearch(publicObjectSearchRequest);
  } catch (e: any) {
    e.message === 'HTTP request failed'
      ? console.error(JSON.stringify(e.response, null, 2))
      : console.error(e)
      return undefined;
  }
  return result;
}

async function createContact(firstName: string, lastName: string, email: string, spaceindustry: string, interestedInMoreInfo:boolean): Promise<string|undefined>{
  var contactId: (string|undefined);
  const contactObj: SimplePublicObjectInputForCreate = {
    properties: {
        firstname: firstName,
        lastname: lastName,
        email: email,
        spaceindustry: spaceindustry,
        tookspaceselfiesmallsat2022: "ImageTaken",
        interestedinmoreinfo: JSON.stringify(interestedInMoreInfo)
    }, associations: []
  }
  try{
    console.log("Creating contact for " + lastName + " " + firstName);
    const createContactResponse = await hubspotClient.crm.contacts.basicApi.create(contactObj);
    contactId = createContactResponse.id;
  } catch(e: any ){
    if(e.body && e.body.message.startsWith("Contact already exists.")){
      const messageArray = e.body.message.replace(/\s/g, "").split(":");
      contactId = messageArray[1];
      console.log("Contact already existing with id " + contactId);
      // update contact with new flags
      const contactUpdateObj: SimplePublicObjectInput = {
        properties: {
          spaceindustry: spaceindustry,
          tookspaceselfiesmallsat2022: "ImageTaken",
          interestedinmoreinfo: JSON.stringify(interestedInMoreInfo)
        }
      }
      await updateContact(contactId!, contactUpdateObj);
    } else {
      console.error(e)
      contactId = undefined;
    }
  }
  return contactId;
}

async function createCompany(companyName: string, contactId: string){
  try {
    console.log("Creating company " + companyName);
    var companyId: (string|undefined) = undefined;
    if(companyName !== undefined && companyName !== ''){
      var companyExists = await doesCompanyAlreadyExist(companyName);
      if(companyExists){
        companyId = (await getCompanyId(companyName));
        console.log("Company already exists with id " + companyId);
      } else {
        const companyObj: SimplePublicObjectInputForCreate = {
          properties: {
            name: companyName,
          }, associations: []
        };
        const createCompanyResponse = await hubspotClient.crm.companies.basicApi.create(companyObj);
        companyId = createCompanyResponse.id;
      }
      if(companyId !== undefined){
        console.log("Associating contact " + contactId + " with company " + companyName);
        const AssociationSpec:AssociationSpec = {associationCategory: "HUBSPOT_DEFINED", associationTypeId: 280}
        await hubspotClient.crm.associations.v4.basicApi.create('company', Number(companyId),'contact', Number(contactId), [AssociationSpec])
      }
    }
  } catch (e: any) {
    console.error(e);
  }
}

async function uploadImage(image: any, lastName: string, firstName: string, companyName: string, contactId: string, now: Moment) : Promise<[string, string]|undefined>{
  try{
    console.log("Uploading image for " + lastName + " " + firstName);
    var folderPath = await createFoldersForUpload(companyName);
    const formData = new FormData();
    const options = {
      "access":  "PUBLIC_NOT_INDEXABLE"
    }
    
    const imageName = getSetting(settings.IMAGE_PREFIX) + "_" + contactId + "_" + now;
    formData.append("folderPath", folderPath);
    formData.append("options", JSON.stringify(options));
    formData.append("file", Buffer.from(image.split('base64,')[1], 'base64'), {"filename": imageName + ".png"});
    const response = await hubspotClient.apiRequest({
      method: 'POST',
      path: '/files/v3/files',
      defaultJson: false,
      body: formData
    });
    var json = await response.json();
    return [json.id, json.url];
  } catch (e: any){
    console.error(e);
    return undefined;
  }
}

function saveImageToDisk(image: string, imageName: string, companyName: string){
  var fs = require('fs');
  const basePath = "spaceSelfieImages/" + getSetting(settings.HUBSPOT_IMAGE_UPLOAD_BASE_FOLDER) + "/" + companyName + "/";
  if(!fs.existsSync(basePath)){
    fs.mkdirSync(basePath, {recursive: true});
  }

  var base64Data = image.replace(/^data:image\/png;base64,/, "");
  fs.writeFile(basePath + imageName + ".png", base64Data, 'base64', (error: any) => {
    if(error){
      return console.log(error);
    }
  });
} 

async function linkImageToContact(imageId: string, contactId: string){
  try {
    console.log("Linking image " + imageId + " to contact " + contactId);
    var body = 
    { engagement: 
        { active: true,
          type: 'NOTE',
          timestamp: Date.now() },
      associations: 
        { contactIds: [ contactId ],
          companyIds: [],
          dealIds: [],
          ownerIds: [] },
      attachments: [ { id: imageId } ],
      metadata: { body: getSetting(settings.HUBSPOT_NOTE_TEXT) }
    };


    const response = await hubspotClient.apiRequest({
      method: "POST",
      path: "/engagements/v1/engagements",
      body: body
    });
    var json = await response.json();
    return json;
  } catch (e: any){
    console.error(e);
    return undefined;
  }
}

async function addImageUrlToContact(contactId: string, imageUrl: string){
  const contactUpdateObj: SimplePublicObjectInput = {
    properties: {
      spaceselfielink: imageUrl
    }
  }
  updateContact(contactId, contactUpdateObj);
}

async function updateContact(contactId: string, contactUpdateObj: SimplePublicObjectInput){
  try{
    const createContactResponse = await hubspotClient.crm.contacts.basicApi.update(contactId, contactUpdateObj);
    console.log(createContactResponse);
  } catch (e: any){
    console.log(e);
  }
}

async function createFoldersForUpload(companyName: string){
  var folderPath: string = getSetting(settings.HUBSPOT_IMAGE_UPLOAD_BASE_FOLDER) + "/" + companyName;
  await createFolderIfNotAlreadyExisting("", folderPath);
  return folderPath;
}

async function createFolderIfNotAlreadyExisting(parentFolderPath: string, folderPath: string){
  var folders: Array<string> = folderPath.split("/");
  if(folderPath.length > 0 && folders.length>0){
    try{

      var folderToCreate = folders[0];
      var folderExists = await doesFolderPathAlreadyExist(folderToCreate);
      if(!folderExists){
        await createFolder(parentFolderPath, folderToCreate)
      }
      var newParentFolderPath = parentFolderPath + "/" + folderToCreate
      folders.shift()
      var newFolderPathToCreate = folders.join("/");
      await createFolderIfNotAlreadyExisting(newParentFolderPath, newFolderPathToCreate)
    } catch (e: any){
      console.error(e);
    }
  }
}

async function createFolder(parentFolderPath: String, folderName: String){
  try{
    console.log("Creating folder " + folderName + " at " + parentFolderPath);
    const body = parentFolderPath === "" ? {"name": folderName} :{"name": folderName, "parentPath": parentFolderPath};
    const response = await hubspotClient.apiRequest({
      method: 'POST',
      path: '/files/v3/folders',
      body: body,
      defaultJson: true,
    });
    const jsonResult = await response.json();
  } catch (e: any){
    console.error(e);
  }
}

async function doesFolderPathAlreadyExist(pathToFolder: string): Promise<Boolean>{
  try {
    console.log("Check if folder exists: " + pathToFolder);
    const response = await hubspotClient.apiRequest({
      method: 'GET',
      path: "/files/v3/folders/search?path=" + encodeURIComponent(pathToFolder),
      defaultJson: true
    })
    const jsonResult = await response.json();
    return jsonResult !== undefined  && jsonResult.length > 0;
  } catch (e: any){
    console.error(e);
    return false
  }
}

async function shortenUrl(imageUrl: string): Promise<string> {

  if(!REBRANDLY_API_KEY){
    console.log("REBRANDLY_API_KEY not found, using original url")
   return imageUrl; 
  }
  console.log("REBRANDLY_API_KEY found, trying to use url-shortener")
  try{
    const headers = {
      "Content-Type": "application/json",
      "apikey": REBRANDLY_API_KEY
    }

    let endpoint = "https://api.rebrandly.com/v1/links";
    let linkRequest = {
      destination: imageUrl,
      domain: { fullName: getSetting(settings.URL_SHORT_DOMAIN) }
    }

    const apiCall = {
      method: 'post',
      url: endpoint,
      data: linkRequest,
      headers: headers
    }

    let apiResponse = await axios(apiCall);
    let link = apiResponse.data;
    var shortenedUrl = !link.shortUrl.startsWith("http") ? "https://" + link.shortUrl: link.shortUrl;
    return shortenedUrl;
  } catch (e: any){
    console.error(e);
    return imageUrl;
  }
}

function saveLocalCopyOfContact(firstName: string, lastName: string, email: string, 
  company: string, spaceindustry: string, interestedInMoreInfo: string, now: Moment, image: string) {
  try{
    const fs = require('fs');
    var contactsPath: any = getSetting(settings.CONTACTS_FOLDER);
    var contactsFile: any = getSetting(settings.CONTACTS_FILE_NAME);
    const CONTACTS_HEADER = "firstName,lastName,email,company,spaceindustry,interestedInMoreInfo,timestamp,imageName\r\n"
    if(!fs.existsSync(contactsPath)){
      fs.mkdirSync(contactsPath, {recursive: true});
      fs.writeFileSync(contactsPath + contactsFile, CONTACTS_HEADER);
    }
    const imageName = getSetting(settings.IMAGE_PREFIX) + "_" + lastName + firstName + "_" + now;
    var csv = firstName+","+lastName+","+email+","+company+","+spaceindustry+","+interestedInMoreInfo+","+now+ "," + imageName +"\r\n";
    fs.appendFileSync(contactsPath + contactsFile, csv);
    
    saveImageToDisk(image, imageName, company);
  } catch(e: any){
    console.log("Saving local copy of Contact failed.");
    console.log(e);
  }
  console.log("Saved local copy of Contact.");
}

function getSetting(setting: string): string{
  let result: any = db.prepare("SELECT value FROM settings WHERE key ='" + setting + "'").get()
  if(result !== undefined|| result !== null){
    return result.value;
  }
  return "";
}
